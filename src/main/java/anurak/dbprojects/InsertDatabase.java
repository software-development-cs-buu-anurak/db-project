package anurak.dbprojects;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import anurak.dbprojects.helper.DatabaseHelper;

public class InsertDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        conn=DatabaseHelper.getConnect();
        //insert prepareStatement
        String sql = "INSERT INTO category(category_id, category_name) VALUES(?, ?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, 4);
            pstmt.setString(2, "Candy");
            pstmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        DatabaseHelper.close();
    }
}

        //insert Statement
//        String sql = "INSERT INTO category(category_id, category_name) VALUES(3, 'Food')";
//        try {
//            Statement stmt = conn.createStatement();
//            int status = stmt.executeUpdate(sql);
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println(""+key.getInt(1));
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
        
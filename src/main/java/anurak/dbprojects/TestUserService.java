package anurak.dbprojects;

import anurak.dbprojects.model.User;
import anurak.dbprojects.service.UserService;

public class TestUserService {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("patchara", "qwer");
        if (user != null) {
            System.out.println("Welcome user: " + user.getName());
        }else{
            System.out.println("Error");
        }
    }
}
